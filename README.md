A probably misguided attempt towards supporting more use of
CLOCK_MONOTONIC in various glibc thread-related functions. The
ultimate goal is to enable C++11's threading primitives to be
implementable in a race-free manner.

References:

* https://sourceware.org/bugzilla/show_bug.cgi?id=14717
* https://gcc.gnu.org/bugzilla/show_bug.cgi?id=41861